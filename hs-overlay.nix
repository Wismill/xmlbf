{ pkgs }:

let hs = pkgs.haskell.lib;
in

# To be used as `packageSetConfig` for a Haskell pacakge set:
self: super:
{
  xmlbf = super.callPackage ./xmlbf/pkg.nix {};
  xmlbf-xeno = super.callPackage ./xmlbf-xeno/pkg.nix {};
  xmlbf-xmlhtml = super.callPackage ./xmlbf-xmlhtml/pkg.nix {};

  _shell = self.shellFor {
    withHoogle = false; # hoogle dependencies don't compile
    packages = p: [
      p.xmlbf
      p.xmlbf-xeno
      p.xmlbf-xmlhtml
    ];
  };

  # deps
  html-entities = hs.overrideCabal super.html-entities (_: {
    postUnpack = "rm -f html-entities-1.1.4.2/Setup.hs";
  });
}
