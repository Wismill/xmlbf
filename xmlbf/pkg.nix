{ mkDerivation, base, bytestring, containers, deepseq, mtl
, QuickCheck, quickcheck-instances, stdenv, tasty, tasty-hunit
, tasty-quickcheck, text, transformers, unordered-containers
}:
mkDerivation {
  pname = "xmlbf";
  version = "0.6";
  src = ./.;
  libraryHaskellDepends = [
    base bytestring containers deepseq mtl text transformers
    unordered-containers
  ];
  testHaskellDepends = [
    base bytestring mtl QuickCheck quickcheck-instances tasty
    tasty-hunit tasty-quickcheck text transformers
  ];
  homepage = "https://gitlab.com/k0001/xmlbf";
  description = "XML back and forth! Parser, renderer, ToXml, FromXml, fixpoints";
  license = stdenv.lib.licenses.asl20;
}
